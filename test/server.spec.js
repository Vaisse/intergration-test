// TDD - integration test
const expect = require("chai").expect;
const request = require("request");
const app = require("../src/server");
const port = 3000;

describe("Color Code Converter API", () => {

    before("Start server before run tests", (done) => {
        server = app.listen(port, () => {
            console.log(`Server listening: localhost:${port}`);
            done();
        });
    });

    describe("HEX to RGB conversion", () => {
        const url = `http://localhost:${port}/hex-to-rgb?h=#3446eb`;

        it("returns status 200", (done) => {
            request(url, (error, response, body) => {
                expect(response.statusCode).to.equal(200);
                done();
            });
        });
        it("returns the color in hex", (done) => {
            request(url, (error, response, body) => {
                expect(body).to.equal("52,70,235");
                done();
            });
        });
    });

    after("Stop server after tests", (done) => {
        server.close();
        done();
    })
});
