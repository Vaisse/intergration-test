// TDD - unit testing

const expect = require("chai").expect;
const converter = require("../src/converter");

describe("Color Code Converter", () => {
    describe("hex to rgb conversion", () => {
        it("converts the basic colors", () => {
            const h = converter.hexToRgb("#3446eb");

            expect(h).to.equal('52,70,235');
        });
    });
});

