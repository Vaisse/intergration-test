const { request } = require('express');
const express = require('express');
const converter = require('./converter');
const app = express();
const port = 3000;

app.get('/', (req, res) => res.send("Hello"));

app.get('/hex-to-rgb', (req, res) => {
    const h = "#3446eb";
    res.send(converter.hexToRgb(h));
});

if (process.env.NODE_ENV === 'test') {
    module.exports = app;
} else {
    app.listen(port, () => console.log(`Server: localhost:${port}`));
}
